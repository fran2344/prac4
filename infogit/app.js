var express = require('express');
var http = require('http');
var io= require('socket.io');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


var routes = require('./routes');
var users = require('./routes/user');
var rutasd=require('./routes/rutas');
var viajesd=require('./routes/viajes'); 

var app = express();
var server=http.createServer(app);
var io=require('socket.io').listen(server);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

app.get('/', routes.index);
app.get('/users', users.list);
app.get('/rutas', rutasd.rutas);
app.get('/viajes', viajesd.viajes);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;

io.sockets.on('connection', function (socket) {
    console.log('A new user connected!');
    socket.on('req_meminfo',function(data){
    //aqui pongo el query
    });
    socket.on('req_cpuinfo',function(data){
    socket.emit('cpuinfo',cpu_info());
    });    
});